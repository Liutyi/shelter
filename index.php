<?php

require "vendor/autoload.php";

$shelter = new Shelter;

//Timestamp захардкодил чтобы наглядно было видно разницу
$shelter->add(new Dog('Atos', 4), 1635795453);
$shelter->add(new Dog('Barsik', 3), 1635795454);
$shelter->add(new Turtle('Storm', 50), 1635795455);
$shelter->add(new Turtle('Fire', 50), 1635795458);
$shelter->add(new Cat('Smarty', 5), 1635795460);
$shelter->add(new Cat('Usik', 2), 1635795457);


echo "Посмотреть всех животных определенного типа, сортированных по кличке в алфавитном порядке";
echo "<pre>";
print_r($shelter->getByType(Cat::class));
echo "</pre>";

echo "Передает человеку животное (определенного типа), находящееся в приюте наибольшее время";
echo "<pre>";
print_r($shelter->giveByType(Dog::class));
echo "</pre>";

echo "Передает человеку животного, находящееся приюте наибольшее время";
echo "<pre>";
print_r($shelter->give());
echo "</pre>";

