<?php

class Shelter
{

    private array $animals = [];
    

    public function add(Animal $animal, int $timestamp)
    {
        $this->animals[$timestamp] = $animal;
        
    }

    public function getByType(string $type)
    {
        $animals = array_filter($this->animals, fn(Animal $animal) => $type === get_class($animal));
        
        usort($animals, fn(Animal $a, Animal $b) => strcmp($a->getName(), $b->getName()));

        return $animals;
        
    }

    public function giveByType(string $type)
    {
        $animals = array_filter($this->animals, fn(Animal $animal) => $type === get_class($animal));
        
        ksort($animals);

        $key = array_key_first($animals);

        unset($this->animals[$key]);

        return $animals[$key];
    }

    public function give()
    {
        ksort($this->animals);

        $key = array_key_first($this->animals);

        $animal = $this->animals[$key];

        unset($this->animals[$key]);

        return $animal;
    }
    
}
